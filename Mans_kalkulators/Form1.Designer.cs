﻿namespace Mans_kalkulators
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_divide = new System.Windows.Forms.Button();
            this.Button_plus = new System.Windows.Forms.Button();
            this.Button_minus = new System.Windows.Forms.Button();
            this.Button_dot = new System.Windows.Forms.Button();
            this.Button_clear = new System.Windows.Forms.Button();
            this.Button_number_0 = new System.Windows.Forms.Button();
            this.Button_number_9 = new System.Windows.Forms.Button();
            this.Button_number_8 = new System.Windows.Forms.Button();
            this.Button_number_7 = new System.Windows.Forms.Button();
            this.Button_number_6 = new System.Windows.Forms.Button();
            this.Button_number_5 = new System.Windows.Forms.Button();
            this.Button_number_4 = new System.Windows.Forms.Button();
            this.Button_number_3 = new System.Windows.Forms.Button();
            this.Button_number_2 = new System.Windows.Forms.Button();
            this.Button_number_1 = new System.Windows.Forms.Button();
            this.Button_times = new System.Windows.Forms.Button();
            this.Button_equals = new System.Windows.Forms.Button();
            this.Equation_box = new System.Windows.Forms.TextBox();
            this.Label_author = new System.Windows.Forms.Label();
            this.Square_root_button = new System.Windows.Forms.Button();
            this.logarithm_button = new System.Windows.Forms.Button();
            this.save_to_txtFile_box = new System.Windows.Forms.CheckBox();
            this.Factorial_button = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.darkModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lightModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EURTextBox = new System.Windows.Forms.TextBox();
            this.GBPTextBox = new System.Windows.Forms.TextBox();
            this.USDTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Convert_button = new System.Windows.Forms.Button();
            this.EURCheckBox = new System.Windows.Forms.CheckBox();
            this.USDCheckBox = new System.Windows.Forms.CheckBox();
            this.GBPCheckBox = new System.Windows.Forms.CheckBox();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Button_divide
            // 
            this.Button_divide.Location = new System.Drawing.Point(539, 224);
            this.Button_divide.Margin = new System.Windows.Forms.Padding(4);
            this.Button_divide.Name = "Button_divide";
            this.Button_divide.Size = new System.Drawing.Size(100, 28);
            this.Button_divide.TabIndex = 2;
            this.Button_divide.Text = "/";
            this.Button_divide.UseVisualStyleBackColor = true;
            this.Button_divide.Click += new System.EventHandler(this.Button_divide_Click);
            // 
            // Button_plus
            // 
            this.Button_plus.Location = new System.Drawing.Point(539, 188);
            this.Button_plus.Margin = new System.Windows.Forms.Padding(4);
            this.Button_plus.Name = "Button_plus";
            this.Button_plus.Size = new System.Drawing.Size(100, 28);
            this.Button_plus.TabIndex = 3;
            this.Button_plus.Text = "+";
            this.Button_plus.UseVisualStyleBackColor = true;
            this.Button_plus.Click += new System.EventHandler(this.Button_plus_Click);
            // 
            // Button_minus
            // 
            this.Button_minus.Location = new System.Drawing.Point(647, 188);
            this.Button_minus.Margin = new System.Windows.Forms.Padding(4);
            this.Button_minus.Name = "Button_minus";
            this.Button_minus.Size = new System.Drawing.Size(100, 28);
            this.Button_minus.TabIndex = 4;
            this.Button_minus.Text = "-";
            this.Button_minus.UseVisualStyleBackColor = true;
            this.Button_minus.Click += new System.EventHandler(this.Button_minus_Click);
            // 
            // Button_dot
            // 
            this.Button_dot.Location = new System.Drawing.Point(172, 295);
            this.Button_dot.Margin = new System.Windows.Forms.Padding(4);
            this.Button_dot.Name = "Button_dot";
            this.Button_dot.Size = new System.Drawing.Size(100, 28);
            this.Button_dot.TabIndex = 5;
            this.Button_dot.Text = ".";
            this.Button_dot.UseVisualStyleBackColor = true;
            this.Button_dot.Click += new System.EventHandler(this.Button_dot_Click);
            // 
            // Button_clear
            // 
            this.Button_clear.Location = new System.Drawing.Point(416, 295);
            this.Button_clear.Margin = new System.Windows.Forms.Padding(4);
            this.Button_clear.Name = "Button_clear";
            this.Button_clear.Size = new System.Drawing.Size(100, 28);
            this.Button_clear.TabIndex = 6;
            this.Button_clear.Text = "C";
            this.Button_clear.UseVisualStyleBackColor = true;
            this.Button_clear.Click += new System.EventHandler(this.Button_clear_Click);
            // 
            // Button_number_0
            // 
            this.Button_number_0.Location = new System.Drawing.Point(293, 295);
            this.Button_number_0.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_0.Name = "Button_number_0";
            this.Button_number_0.Size = new System.Drawing.Size(100, 28);
            this.Button_number_0.TabIndex = 7;
            this.Button_number_0.Text = "0";
            this.Button_number_0.UseVisualStyleBackColor = true;
            this.Button_number_0.Click += new System.EventHandler(this.Button_number_0_Click);
            // 
            // Button_number_9
            // 
            this.Button_number_9.Location = new System.Drawing.Point(416, 260);
            this.Button_number_9.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_9.Name = "Button_number_9";
            this.Button_number_9.Size = new System.Drawing.Size(100, 28);
            this.Button_number_9.TabIndex = 8;
            this.Button_number_9.Text = "9";
            this.Button_number_9.UseVisualStyleBackColor = true;
            this.Button_number_9.Click += new System.EventHandler(this.Button_number_9_Click);
            // 
            // Button_number_8
            // 
            this.Button_number_8.Location = new System.Drawing.Point(293, 260);
            this.Button_number_8.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_8.Name = "Button_number_8";
            this.Button_number_8.Size = new System.Drawing.Size(100, 28);
            this.Button_number_8.TabIndex = 9;
            this.Button_number_8.Text = "8";
            this.Button_number_8.UseVisualStyleBackColor = true;
            this.Button_number_8.Click += new System.EventHandler(this.Button_number_8_Click);
            // 
            // Button_number_7
            // 
            this.Button_number_7.Location = new System.Drawing.Point(172, 260);
            this.Button_number_7.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_7.Name = "Button_number_7";
            this.Button_number_7.Size = new System.Drawing.Size(100, 28);
            this.Button_number_7.TabIndex = 10;
            this.Button_number_7.Text = "7";
            this.Button_number_7.UseVisualStyleBackColor = true;
            this.Button_number_7.Click += new System.EventHandler(this.Button_number_7_Click);
            // 
            // Button_number_6
            // 
            this.Button_number_6.Location = new System.Drawing.Point(416, 224);
            this.Button_number_6.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_6.Name = "Button_number_6";
            this.Button_number_6.Size = new System.Drawing.Size(100, 28);
            this.Button_number_6.TabIndex = 11;
            this.Button_number_6.Text = "6";
            this.Button_number_6.UseVisualStyleBackColor = true;
            this.Button_number_6.Click += new System.EventHandler(this.Button_number_6_Click);
            // 
            // Button_number_5
            // 
            this.Button_number_5.Location = new System.Drawing.Point(293, 224);
            this.Button_number_5.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_5.Name = "Button_number_5";
            this.Button_number_5.Size = new System.Drawing.Size(100, 28);
            this.Button_number_5.TabIndex = 12;
            this.Button_number_5.Text = "5";
            this.Button_number_5.UseVisualStyleBackColor = true;
            this.Button_number_5.Click += new System.EventHandler(this.Button_number_5_Click);
            // 
            // Button_number_4
            // 
            this.Button_number_4.Location = new System.Drawing.Point(172, 224);
            this.Button_number_4.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_4.Name = "Button_number_4";
            this.Button_number_4.Size = new System.Drawing.Size(100, 28);
            this.Button_number_4.TabIndex = 13;
            this.Button_number_4.Text = "4";
            this.Button_number_4.UseVisualStyleBackColor = true;
            this.Button_number_4.Click += new System.EventHandler(this.Button_number_4_Click);
            // 
            // Button_number_3
            // 
            this.Button_number_3.Location = new System.Drawing.Point(416, 188);
            this.Button_number_3.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_3.Name = "Button_number_3";
            this.Button_number_3.Size = new System.Drawing.Size(100, 28);
            this.Button_number_3.TabIndex = 14;
            this.Button_number_3.Text = "3";
            this.Button_number_3.UseVisualStyleBackColor = true;
            this.Button_number_3.Click += new System.EventHandler(this.Button_number_3_Click);
            // 
            // Button_number_2
            // 
            this.Button_number_2.Location = new System.Drawing.Point(293, 188);
            this.Button_number_2.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_2.Name = "Button_number_2";
            this.Button_number_2.Size = new System.Drawing.Size(100, 28);
            this.Button_number_2.TabIndex = 15;
            this.Button_number_2.Text = "2";
            this.Button_number_2.UseVisualStyleBackColor = true;
            this.Button_number_2.Click += new System.EventHandler(this.Button_number_2_Click);
            // 
            // Button_number_1
            // 
            this.Button_number_1.Location = new System.Drawing.Point(172, 188);
            this.Button_number_1.Margin = new System.Windows.Forms.Padding(4);
            this.Button_number_1.Name = "Button_number_1";
            this.Button_number_1.Size = new System.Drawing.Size(100, 28);
            this.Button_number_1.TabIndex = 16;
            this.Button_number_1.Text = "1";
            this.Button_number_1.UseVisualStyleBackColor = true;
            this.Button_number_1.Click += new System.EventHandler(this.Button_number_1_Click);
            // 
            // Button_times
            // 
            this.Button_times.Location = new System.Drawing.Point(647, 224);
            this.Button_times.Margin = new System.Windows.Forms.Padding(4);
            this.Button_times.Name = "Button_times";
            this.Button_times.Size = new System.Drawing.Size(100, 28);
            this.Button_times.TabIndex = 17;
            this.Button_times.Text = "*";
            this.Button_times.UseVisualStyleBackColor = true;
            this.Button_times.Click += new System.EventHandler(this.Button_times_Click);
            // 
            // Button_equals
            // 
            this.Button_equals.Location = new System.Drawing.Point(647, 295);
            this.Button_equals.Margin = new System.Windows.Forms.Padding(4);
            this.Button_equals.Name = "Button_equals";
            this.Button_equals.Size = new System.Drawing.Size(100, 28);
            this.Button_equals.TabIndex = 18;
            this.Button_equals.Text = "=";
            this.Button_equals.UseVisualStyleBackColor = true;
            this.Button_equals.Click += new System.EventHandler(this.Button_equals_Click);
            // 
            // Equation_box
            // 
            this.Equation_box.Location = new System.Drawing.Point(172, 110);
            this.Equation_box.Margin = new System.Windows.Forms.Padding(4);
            this.Equation_box.Multiline = true;
            this.Equation_box.Name = "Equation_box";
            this.Equation_box.Size = new System.Drawing.Size(343, 70);
            this.Equation_box.TabIndex = 19;
            // 
            // Label_author
            // 
            this.Label_author.AutoSize = true;
            this.Label_author.Location = new System.Drawing.Point(865, 31);
            this.Label_author.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label_author.Name = "Label_author";
            this.Label_author.Size = new System.Drawing.Size(163, 17);
            this.Label_author.TabIndex = 20;
            this.Label_author.Text = "Kristaps Blumbergs 3ITB";
            // 
            // Square_root_button
            // 
            this.Square_root_button.Location = new System.Drawing.Point(539, 260);
            this.Square_root_button.Margin = new System.Windows.Forms.Padding(4);
            this.Square_root_button.Name = "Square_root_button";
            this.Square_root_button.Size = new System.Drawing.Size(100, 28);
            this.Square_root_button.TabIndex = 24;
            this.Square_root_button.Text = "√";
            this.Square_root_button.UseVisualStyleBackColor = true;
            this.Square_root_button.Click += new System.EventHandler(this.Square_root_button_Click);
            // 
            // logarithm_button
            // 
            this.logarithm_button.Location = new System.Drawing.Point(647, 260);
            this.logarithm_button.Margin = new System.Windows.Forms.Padding(4);
            this.logarithm_button.Name = "logarithm_button";
            this.logarithm_button.Size = new System.Drawing.Size(100, 28);
            this.logarithm_button.TabIndex = 25;
            this.logarithm_button.Text = "ln";
            this.logarithm_button.UseVisualStyleBackColor = true;
            this.logarithm_button.Click += new System.EventHandler(this.Logarithm_button_Click);
            // 
            // save_to_txtFile_box
            // 
            this.save_to_txtFile_box.AutoSize = true;
            this.save_to_txtFile_box.Location = new System.Drawing.Point(539, 160);
            this.save_to_txtFile_box.Margin = new System.Windows.Forms.Padding(4);
            this.save_to_txtFile_box.Name = "save_to_txtFile_box";
            this.save_to_txtFile_box.Size = new System.Drawing.Size(82, 21);
            this.save_to_txtFile_box.TabIndex = 26;
            this.save_to_txtFile_box.Text = "save .txt";
            this.save_to_txtFile_box.UseVisualStyleBackColor = true;
            // 
            // Factorial_button
            // 
            this.Factorial_button.Location = new System.Drawing.Point(539, 294);
            this.Factorial_button.Margin = new System.Windows.Forms.Padding(4);
            this.Factorial_button.Name = "Factorial_button";
            this.Factorial_button.Size = new System.Drawing.Size(100, 28);
            this.Factorial_button.TabIndex = 27;
            this.Factorial_button.Text = "!";
            this.Factorial_button.UseVisualStyleBackColor = true;
            this.Factorial_button.Click += new System.EventHandler(this.Factorial_button_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.infoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1074, 28);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.FileToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.QuitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.darkModeToolStripMenuItem,
            this.lightModeToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // darkModeToolStripMenuItem
            // 
            this.darkModeToolStripMenuItem.Name = "darkModeToolStripMenuItem";
            this.darkModeToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
            this.darkModeToolStripMenuItem.Text = "Dark mode";
            this.darkModeToolStripMenuItem.Click += new System.EventHandler(this.DarkModeToolStripMenuItem_Click);
            // 
            // lightModeToolStripMenuItem
            // 
            this.lightModeToolStripMenuItem.Name = "lightModeToolStripMenuItem";
            this.lightModeToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
            this.lightModeToolStripMenuItem.Text = "Light mode";
            this.lightModeToolStripMenuItem.Click += new System.EventHandler(this.LightModeToolStripMenuItem_Click);
            // 
            // EURTextBox
            // 
            this.EURTextBox.Location = new System.Drawing.Point(854, 94);
            this.EURTextBox.Name = "EURTextBox";
            this.EURTextBox.Size = new System.Drawing.Size(100, 22);
            this.EURTextBox.TabIndex = 29;
            // 
            // GBPTextBox
            // 
            this.GBPTextBox.Location = new System.Drawing.Point(854, 188);
            this.GBPTextBox.Name = "GBPTextBox";
            this.GBPTextBox.Size = new System.Drawing.Size(100, 22);
            this.GBPTextBox.TabIndex = 30;
            // 
            // USDTextBox
            // 
            this.USDTextBox.Location = new System.Drawing.Point(854, 140);
            this.USDTextBox.Name = "USDTextBox";
            this.USDTextBox.Size = new System.Drawing.Size(100, 22);
            this.USDTextBox.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(811, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 32;
            this.label1.Text = "EUR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(811, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 17);
            this.label2.TabIndex = 33;
            this.label2.Text = "USD";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(811, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 17);
            this.label3.TabIndex = 34;
            this.label3.Text = "GBP";
            // 
            // Convert_button
            // 
            this.Convert_button.Location = new System.Drawing.Point(878, 228);
            this.Convert_button.Name = "Convert_button";
            this.Convert_button.Size = new System.Drawing.Size(75, 34);
            this.Convert_button.TabIndex = 35;
            this.Convert_button.Text = "Convert";
            this.Convert_button.UseVisualStyleBackColor = true;
            this.Convert_button.Click += new System.EventHandler(this.Convert_button_Click);
            // 
            // EURCheckBox
            // 
            this.EURCheckBox.AutoSize = true;
            this.EURCheckBox.Location = new System.Drawing.Point(960, 100);
            this.EURCheckBox.Name = "EURCheckBox";
            this.EURCheckBox.Size = new System.Drawing.Size(18, 17);
            this.EURCheckBox.TabIndex = 36;
            this.EURCheckBox.UseVisualStyleBackColor = true;
            // 
            // USDCheckBox
            // 
            this.USDCheckBox.AutoSize = true;
            this.USDCheckBox.Location = new System.Drawing.Point(960, 146);
            this.USDCheckBox.Name = "USDCheckBox";
            this.USDCheckBox.Size = new System.Drawing.Size(18, 17);
            this.USDCheckBox.TabIndex = 37;
            this.USDCheckBox.UseVisualStyleBackColor = true;
            // 
            // GBPCheckBox
            // 
            this.GBPCheckBox.AutoSize = true;
            this.GBPCheckBox.Location = new System.Drawing.Point(960, 195);
            this.GBPCheckBox.Name = "GBPCheckBox";
            this.GBPCheckBox.Size = new System.Drawing.Size(18, 17);
            this.GBPCheckBox.TabIndex = 38;
            this.GBPCheckBox.UseVisualStyleBackColor = true;
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(49, 24);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 554);
            this.Controls.Add(this.GBPCheckBox);
            this.Controls.Add(this.USDCheckBox);
            this.Controls.Add(this.EURCheckBox);
            this.Controls.Add(this.Convert_button);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.USDTextBox);
            this.Controls.Add(this.GBPTextBox);
            this.Controls.Add(this.EURTextBox);
            this.Controls.Add(this.Factorial_button);
            this.Controls.Add(this.save_to_txtFile_box);
            this.Controls.Add(this.logarithm_button);
            this.Controls.Add(this.Square_root_button);
            this.Controls.Add(this.Label_author);
            this.Controls.Add(this.Equation_box);
            this.Controls.Add(this.Button_equals);
            this.Controls.Add(this.Button_times);
            this.Controls.Add(this.Button_number_1);
            this.Controls.Add(this.Button_number_2);
            this.Controls.Add(this.Button_number_3);
            this.Controls.Add(this.Button_number_4);
            this.Controls.Add(this.Button_number_5);
            this.Controls.Add(this.Button_number_6);
            this.Controls.Add(this.Button_number_7);
            this.Controls.Add(this.Button_number_8);
            this.Controls.Add(this.Button_number_9);
            this.Controls.Add(this.Button_number_0);
            this.Controls.Add(this.Button_clear);
            this.Controls.Add(this.Button_dot);
            this.Controls.Add(this.Button_minus);
            this.Controls.Add(this.Button_plus);
            this.Controls.Add(this.Button_divide);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Button Button_divide;
        internal System.Windows.Forms.Button Button_plus;
        internal System.Windows.Forms.Button Button_minus;
        internal System.Windows.Forms.Button Button_dot;
        internal System.Windows.Forms.Button Button_clear;
        internal System.Windows.Forms.Button Button_number_0;
        internal System.Windows.Forms.Button Button_number_9;
        internal System.Windows.Forms.Button Button_number_8;
        internal System.Windows.Forms.Button Button_number_7;
        internal System.Windows.Forms.Button Button_number_6;
        internal System.Windows.Forms.Button Button_number_5;
        internal System.Windows.Forms.Button Button_number_4;
        internal System.Windows.Forms.Button Button_number_3;
        internal System.Windows.Forms.Button Button_number_2;
        internal System.Windows.Forms.Button Button_number_1;
        internal System.Windows.Forms.Button Button_times;
        internal System.Windows.Forms.Button Button_equals;
        private System.Windows.Forms.TextBox Equation_box;
        private System.Windows.Forms.Label Label_author;
        private System.Windows.Forms.Button Square_root_button;
        private System.Windows.Forms.Button logarithm_button;
        private System.Windows.Forms.CheckBox save_to_txtFile_box;
        private System.Windows.Forms.Button Factorial_button;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem darkModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lightModeToolStripMenuItem;
        private System.Windows.Forms.TextBox EURTextBox;
        private System.Windows.Forms.TextBox GBPTextBox;
        private System.Windows.Forms.TextBox USDTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Convert_button;
        private System.Windows.Forms.CheckBox EURCheckBox;
        private System.Windows.Forms.CheckBox USDCheckBox;
        private System.Windows.Forms.CheckBox GBPCheckBox;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
    }
}

