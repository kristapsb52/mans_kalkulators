﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace Mans_kalkulators
{

    public partial class Form1 : Form
    {
        public String fileName = Directory.GetCurrentDirectory() + "\\log.txt";
        Boolean dotPressed = false;
        double firstDigit;
        string secondDigit;
        string operation;
        int charactersForDigit = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Button_number_1_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "1";
            }
            Equation_box.Text = Equation_box.Text + "1";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_2_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "2";
            }
            Equation_box.Text = Equation_box.Text + "2";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_3_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "3";
            }
            Equation_box.Text = Equation_box.Text + "3";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_4_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "4";
            }
            Equation_box.Text = Equation_box.Text + "4";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_5_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "5";
            }
            Equation_box.Text = Equation_box.Text + "5";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_6_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "6";
            }
            Equation_box.Text = Equation_box.Text + "6";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_7_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "7";
            }
            Equation_box.Text = Equation_box.Text + "7";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_8_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "8";
            }
            Equation_box.Text = Equation_box.Text + "8";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_number_9_Click(object sender, EventArgs e)
        {
            if (operation != null)
            {
                if (charactersForDigit == 0)
                {
                    Equation_box.Text = null;
                }
                secondDigit = secondDigit + "9";
            }
            Equation_box.Text = Equation_box.Text + "9";

            charactersForDigit = charactersForDigit + 1;
        }

        private void Button_dot_Click(object sender, EventArgs e)
        {
            string temp = Equation_box.Text;

            if (charactersForDigit != 0)
            {
                if (operation != null && dotPressed == false)
                {
                    secondDigit = secondDigit + ".";
                    Equation_box.Text = Equation_box.Text + ".";
                    dotPressed = true;
                }

                else if (dotPressed == false && operation == null)
                {
                    Equation_box.Text = Equation_box.Text + ".";
                    dotPressed = true;
                }
            }

        }

        private void Button_number_0_Click(object sender, EventArgs e)
        {
            if (charactersForDigit != 0)
            {
                Equation_box.Text = Equation_box.Text + "0";

                charactersForDigit = charactersForDigit + 1;
            }
            if (operation != null)
            {
                secondDigit = secondDigit + "0";
            }
        }

        private void Button_clear_Click(object sender, EventArgs e)
        {
            firstDigit = 0;
            secondDigit = null;
            Equation_box.Text = null;
            operation = null;
            dotPressed = false;
            charactersForDigit = 0;
        }

        private void Button_plus_Click(object sender, EventArgs e)
        {
            secondDigit = null;
            if (firstDigit == 0)
            {
                string tempString = Equation_box.Text;
                firstDigit = Convert.ToDouble(tempString);
            }

            operation = "+";

            dotPressed = false;

            charactersForDigit = 0;
        }

        private void Button_minus_Click(object sender, EventArgs e)
        {
            secondDigit = null;
            if (firstDigit == 0)
            {
                string tempString = Equation_box.Text;
                firstDigit = Convert.ToDouble(tempString);
            }

            operation = "-";

            dotPressed = false;
            charactersForDigit = 0;
        }

        private void Button_divide_Click(object sender, EventArgs e)
        {
            secondDigit = null;
            if (firstDigit == 0)
            {
                string tempString = Equation_box.Text;
                firstDigit = Convert.ToDouble(tempString);
            }

            operation = "/";

            dotPressed = false;
            charactersForDigit = 0;
        }

        private void Button_times_Click(object sender, EventArgs e)
        {
            secondDigit = null;
            if (firstDigit == 0)
            {
                string tempString = Equation_box.Text;
                firstDigit = Convert.ToDouble(tempString);
            }

            operation = "*";

            dotPressed = false;
            charactersForDigit = 0;
        }

        private void Button_equals_Click(object sender, EventArgs e)
        {
            double secondDigitDouble = Convert.ToDouble(secondDigit);

            double result = 0;
            if (operation == "+")
            {
                result = firstDigit + secondDigitDouble;
            }
            else if (operation == "-")
            {
                result = firstDigit - secondDigitDouble;
            }
            else if (operation == "/")
            {
                result = firstDigit / secondDigitDouble;
            }
            else if (operation == "*")
            {
                result = firstDigit * secondDigitDouble;
            }
            else if (operation == "ln")
            {
                result = Math.Log(secondDigitDouble);
            }
            else if (operation == "sqrt")
            {
                result = Math.Sqrt(secondDigitDouble);
            }
            else if (operation == "!")
            {
                if (dotPressed == false)
                {
                    if (secondDigitDouble != 0)
                    {
                        result = 1;
                        for (int i = 2; i <= secondDigitDouble; i++)
                        {
                            result = result * i;
                        }
                    }
                }
            }
            string resultString = result.ToString();
            Equation_box.Text = null;

            Equation_box.Text = Equation_box.Text + resultString;

            if (save_to_txtFile_box.Checked)
            {
                //if its empty
                if (!File.Exists(fileName))
                {
                    using (StreamWriter sw = File.CreateText(fileName))
                    {
                        if (operation == "ln")
                        {
                            sw.WriteLine($"ln({secondDigitDouble}) = {result}");
                        }
                        else if(operation == "sqrt")
                        {
                            sw.WriteLine($"sqrt({secondDigitDouble}) = {result}");
                        }
                        else if(operation == "!")
                        {
                            sw.WriteLine($"!({secondDigitDouble}) = {result}");
                        }
                        else
                        {
                            sw.WriteLine($"{firstDigit} {operation} {secondDigit} = {result} \n");
                        }
                    }
                }

                else if (new FileInfo(fileName).Length == 0) {
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        if (operation == "ln")
                        {
                            sw.WriteLine($"ln({secondDigitDouble}) = {result}");
                        }
                        else if (operation == "sqrt")
                        {
                            sw.WriteLine($"sqrt({secondDigitDouble}) = {result}");
                        }
                        else if (operation == "!")
                        {
                            sw.WriteLine($"!({secondDigitDouble}) = {result}");
                        }
                        else
                        {
                            sw.WriteLine($"{firstDigit} {operation} {secondDigit} = {result} \n");
                        }
                    }
                }
                //if the file does have lines in it
                else
                {
                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        if (operation == "ln")
                        {
                            sw.WriteLine($"ln({secondDigitDouble}) = {result}");
                        }
                        else if (operation == "sqrt")
                        {
                            sw.WriteLine($"sqrt({secondDigitDouble}) = {result}");
                        }
                        else if (operation == "!")
                        {
                            sw.WriteLine($"!({secondDigitDouble}) = {result}");
                        }
                        else
                        {
                            sw.WriteLine($"{firstDigit} {operation} {secondDigit} = {result} \n");
                        }
                    }
                }

            }
            firstDigit = result;
            dotPressed = false;

            charactersForDigit = 0;

        }

        private void Square_root_button_Click(object sender, EventArgs e)
        {
            Equation_box.Text = "sqrt(";
            operation = "sqrt";
            secondDigit = null;
        }

        private void Logarithm_button_Click(object sender, EventArgs e)
        {
            Equation_box.Text = "ln(";
            operation = "ln";
            secondDigit = null;
        }

        private void Factorial_button_Click(object sender, EventArgs e)
        {
            Equation_box.Text = "!";
            operation = "!";
            secondDigit = null;
        }

        private void FileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void QuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "Are you sure you want to quit?";
            string caption = "Form Closing";
            var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void DarkModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lightModeToolStripMenuItem.Checked == true)
            {
                lightModeToolStripMenuItem.Checked = false;
            }
            darkModeToolStripMenuItem.Checked = true;
            this.BackColor = Color.FromArgb(17, 76, 113);
        }

        private void LightModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(darkModeToolStripMenuItem.Checked == true)
            {
                darkModeToolStripMenuItem.Checked = false;
            }
            lightModeToolStripMenuItem.Checked = true;
            this.BackColor = Color.FromArgb(255, 255, 255);
        }

        private void Convert_button_Click(object sender, EventArgs e)
        {
            if(EURCheckBox.Checked)
            {
                ///Convert values to USD and GBP
                try
                {
                    double eurValue = Convert.ToDouble(EURTextBox.Text);
                    double usdValue = eurValue * 1.09308;
                    double gbpValue = eurValue * 0.891037294;

                    GBPTextBox.Text = null;
                    GBPTextBox.Text = gbpValue.ToString();

                    USDTextBox.Text = null;
                    USDTextBox.Text = usdValue.ToString();
                }
                catch
                {
                    string message = "Invalid value";
                    string caption = "Error";
                    var result = MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (USDCheckBox.Checked)
            {
                ///Convert values to EUR and GBP
                try 
                {
                    double usdValue = Convert.ToDouble(USDTextBox.Text);
                    double eurValue = usdValue * 0.914846123;
                    double gbpValue = usdValue * 0.815162013;

                    GBPTextBox.Text = null;
                    GBPTextBox.Text = gbpValue.ToString();

                    EURTextBox.Text = null;
                    EURTextBox.Text = eurValue.ToString();
                }
                catch
                {
                    string message = "Invalid value";
                    string caption = "Error";
                    var result = MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else if(GBPCheckBox.Checked)
            {
                ///Convert values to EUR and USD
                try
                {
                    double gbpValue = Convert.ToDouble(GBPTextBox.Text);
                    double eurValue = gbpValue * 1.12203342;
                    double usdValue = gbpValue * 1.226865;

                    USDTextBox.Text = null;
                    USDTextBox.Text = usdValue.ToString();

                    EURTextBox.Text = null;
                    EURTextBox.Text = eurValue.ToString();
                }
                catch
                {
                    string message = "Invalid value";
                    string caption = "Error";
                    var result = MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            File.WriteAllText(this.fileName, String.Empty);
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = "This calculator was made by Kristaps Blumbergs 3rd course Computer Science!";
            string caption = "Information";
            var result = MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
